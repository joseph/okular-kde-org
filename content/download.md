---
layout: download
title: Download
appstream: org.kde.okular
name: Okular
menu:
  main:
    parent: about
    weight: 3
description: Okular is available as a precompiled package in a wide range of platforms. You can check the package status for your Linux distro on the right or keep reading for info on other operating systems
scssFiles:
- /scss/download.scss
sources:
- name: Release Sources
  src_icon: /reusable-assets/ark.svg
  description: "Okular is released regularly as part of KDE Gear. If you want to build from source, you can check the [Build It section](/build-it)."
- name: Windows
  src_icon: /reusable-assets/windows.svg
  description: "The [Microsoft Store](https://www.microsoft.com/store/apps/9n41msq1wnm8) is the recommended place to install Okular, the version there has been tested by our developers and the Microsoft Store provides seamless updates when new versions are released."
---
