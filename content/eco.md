---
title: How is Okular Subtainable?
layout: eco
sassFiles:
  - /scss/eco.scss

cards:
  - title: Eco-Certification
    icon: eco-certification-icon.svg
    description: |
      Blue Angel certified for transparency in energy use, a longer hardware operating life, and user freedoms.
    link: '#eco-certification'

  - title: Energy Efficiency
    icon: energy-efficiency-icon.svg
    description: |
      Software efficiency determines the energy demands and operating life of digital infrastructure.
    link: '#energy-efficiency'

  - title: Transparency & Autonomy
    icon: user-autonomy-icon.svg
    link: '#user-autonomy'
    description: |
      A Free Software license makes for the efficient, long-term, and sustainable use of hardware.

  - title: Ethics & Privacy
    icon: ethics-and-privacy-icon.svg
    description: |
      The [four freedoms](https://fsfe.org/freesoftware/index.en.html) are critical for an inclusive, equitable, and ecologically sustainable digitization.

card_copyright: |
  Medal by Ongycon, Green Lamp by Stefania Servidio, User by Guru, Ethics by
  Ruben Hojo from [Noun Project](https://thenounproject.com/) (CC BY 3.0)

certification:
  text: Eco-Certification
  description: |
    Okular is proud to be the first computer software worldwide to have
    received the Blue Angel seal in 2022. Introduced in 1978, the Blue Angel is
    the first ever ecolabel worldwide and the official environmental label of
    the German government. In 2020, the German Environment Agency
    (Umweltbundesamt) released the award criteria for desktop software with
    three main categories: (A) Resource & Energy Efficiency, (B) Potential
    Hardware Operating Life, and (C) User Autonomy. Software products that
    demonstrate compliance with these criteria enable longer and more efficient
    use of hardware.

    Interested to know more? Check out the KDE Eco handbook ["Applying The Blue
    Angel Criteria To Free Software"](https://eco.kde.org/handbook/).

energy:
  text: Energy Efficiency
  description: |
    Download the most recent energy consumption report for Okular
    [here](https://invent.kde.org/websites/okular-kde-org/-/blob/master/NEED%20LINK).
    Software design and implementation have a significant impact on the energy
    consumption of the systems it is a part of. By becoming aware of how
    software affects energy consumption and providing tools to quantify them,
    it is possible to drive down energy consumption and increase efficiency.
    This contributes to a more sustainable use of energy as one of the shared
    resources of our planet.

    Learn more about KDE's work on energy efficiency and sustainable software
    at the [KDE Eco](https://eco.kde.org/) website.

user:
  text: Transparency & Autonomy
  description: |
    KDE wants to [put you in the driver's seat](https://community.kde.org/KDE/Vision) with all of its software, including Okular. Okular is available under a [Free Software license](https://invent.kde.org/graphics/okular/-/blob/master/COPYING), and the source code is readily available at [Okular's repository](https://invent.kde.org/graphics/okular). With Okular's [data format documentation](https://okular.kde.org/formats), you will never again be subject to vendor lock in from proprietary formats. Moreover, enhancing Okular's functionality is possible with its [API Documentation](https://api.kde.org/okular/html/index.html). [Install and uninstall instructions](https://userbase.kde.org/Tutorials/Install_KDE_software) are available to make sure users are not burdened by unwanted software components on their machines. And for those who want long-term support for their favorite software, Okular's [full list of release announcements](https://okular.kde.org/news/) ǵoes all the way back to 2006 and will continue well into the future.

    By guaranteeing transparency and autonomy, developers and users can ensure
    that their software reduces environmental harm in more ways than one,
    whether by keeping devices in use for longer, or by reducing the software’s
    use of energy and resources while in use.

ethics:
  text: Ethics & Privacy
  description: |
    Being Free & Open Source Software, Okular's source code is available for
    all to use, study, share, improve, and most of all, enjoy. KDE software
    will always be available to all without being restricted to materially,
    educationally, or socially privileged people. [KDE's Code of
    Conduct](https://kde.org/code-of-conduct/) provides guidelines to support
    everyone in the community for a positive and inspiring atmosphere. This is
    necessary for software development that meets all its users' needs,
    wherever they are, on whatever device they own -- and not just ones vendors
    want to sell.

    KDE [envisions](https://community.kde.org/KDE/Vision) a "world in which
    everyone has control over their digital life and enjoys freedom and
    privacy." Freedom from unethical practices such as unwanted data collection
    and tracking or [psychological
    targeting](https://en.wikipedia.org/wiki/Facebook%E2%80%93Cambridge_Analytica_data_scandal)
    is not only good for society, but it also reduces the carbon footprint of
    digital technology. A default opt out for ads and telemetry decreases
    energy demands on both end-user devices and servers running the ads or
    mining the data, and it reduces the energy needed for network traffic.
    [KDE's Privacy Policy](https://kde.org/privacypolicy-apps/) ensures that
    its software does not collect any information from users except when users
    actively and explicitly opt in to it!

    KDE is [software for you](https://kde.org/for/), by you!

eco:
  text: Want to learn more about sustainable software?
  description: |
    KDE has the [goal](https://community.kde.org/Goals/Sustainable_Software) of providing software that reduces software's environmental impact. Let's make energy efficient software and digital sustainability part of the Free Software community. We can live up to our responsibility for this and future generations!
path:
  title: Okular's Path Toward Sustainability
  description1: |
    In 2022 Okular was the awarded the [Blue Angel ecolabel for sustainable
    software
    design](https://www.blauer-engel.de/en/productworld/resources-and-energy-efficient-software-products),
    becoming the first eco-certified computer program within the Global
    Ecolabelling Network.

  alt: Okular was awarded the Blue Angel ecolabel in 2022.

  description2: |
    With KDE’s long-standing [mission](https://community.kde.org/KDE/Mission)
    and guiding [vision](https://community.kde.org/KDE/Vision) since its
    founding in 1996, as well as the talent and capabilities of its community
    members, KDE is a pioneer in championing sustainable software. In 2021 KDE
    started [KDE Eco](https://eco.kde.org/), a project with the goal of putting
    KDE and Free Software at the forefront of sustainable software engineering,
    and in 2022 the KDE community voted to have [Sustainable
    Software](https://community.kde.org/Goals/Sustainable_Software) as one of
    its overarching goals.

    Sustainability is not new for Free & Open Source Software (FOSS)&mdash;the
    [four freedoms](https://fsfe.org/freesoftware/index.en.html) have always
    made [Free Software sustainable
    software](https://fsfe.org/freesoftware/sustainability/). But now, the two
    pillars of FOSS&mdash;transparency and user autonomy&mdash;have wider
    recognition for their impacts on sustainability, and were incorporated into
    the sustainability criteria set by the German Environment Agency through
    the Blue Angel ecolabel.

  quote_de: |
    "Viele machen sich nicht bewusst, dass Software dazu beitragen kann,
    dass Hardware ausgemustert werden muss und so unnötig Ressourcen
    verschwendet."

  quote_en: |
    <em>Many people don't realize that software can contribute to hardware having
    to be decommissioned, thus wasting resources unnecessarily.</em>
---


Free & Open Source Software guarantees transparency and hands control over to users, rather than obligating them to work with certain vendors or service providers. This allows users to decide what they want from the software they use and, in turn, make decisions about the hardware they use as well. Users are able to reduce the energy consumption of their programs with little or no loss in functionality, installing only what they need, no more and no less; they can also avoid invasive advertising or data-mining options which run processes in the background, further consuming resources on the device and in the network. As for FOSS developers, they typically continue to support hardware that the industry would be eager to make obsolete, providing users with up-to-date and secure software for devices that might otherwise be discarded as e-waste and end up polluting landfills.

