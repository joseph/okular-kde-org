---
date: 2016-08-18
title: Okular 0.26 released
---
The 0.26 version of Okular has been released together with KDE Applications 16.08 release. This release introduces very small changes, you can check the full changelog at <a href='https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular'>https://www.kde.org/announcements/fulllog_applications.php?version=16.08.0#okular</a>. Okular 0.26 is a recommended update for everyone using Okular.