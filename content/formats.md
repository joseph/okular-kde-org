---
layout: formats
title: Document Format Handlers Status
intro: Okular supports a wide variety of document formats and use cases.
menu:
  main:
   parent: about
   weight: 1
   name: Document Format
sassFiles:
  - /scss/table.scss
---
